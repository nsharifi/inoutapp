package org.jhpiego.inout.http;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.jhpiego.inout.EmployeesSelection;
import org.jhpiego.inout.R;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class UsersDownloadTask extends AsyncTask<String, Integer, String> {
	private EmployeesSelection activity;
//	private AutoCompleteTextView autoCompleteText;
	private TableLayout table;
	private JSONArray jsonUsers;
	private String[] userArrayAdapter;
	private int userId;
	private ProgressDialog progressDialog;
	
	public UsersDownloadTask(EmployeesSelection activity) {
		this.activity = activity;
		progressDialog = new ProgressDialog(activity);
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		
	};
	
	@Override
	protected String doInBackground(String... urls) {
		String urlString = urls[0];
		StringBuffer output = new StringBuffer();
		try {
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			conn.connect();			
//			int responseCode = conn.getResponseCode();
//			Log.e("INOUT", "Response code is: " + responseCode);
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				output.append(line);
			}
		} catch (Exception e) {
			Log.e("INOUT", "Couldn't get data from server: " + e);
		}

		return output.toString();
	}
	
	@Override
	protected void onPreExecute() {
		progressDialog.setMessage("Processing...");
		progressDialog.show();
	}
	@Override
	protected void onPostExecute(String result) {
		//autoCompleteText = (AutoCompleteTextView)activity.findViewById(R.id.staff_autocomplete);
		table = (TableLayout)activity.findViewById(R.id.users_table);
		progressDialog.dismiss();
		
		try {
			jsonUsers = new JSONArray(result);
			userArrayAdapter = new String[jsonUsers.length()];
			for(int i = 0; i < jsonUsers.length(); i++) {
				JSONObject jsonUser = jsonUsers.getJSONObject(i);
				JSONObject jsonUserStatus = new JSONObject(jsonUser.getString("status"));
				userArrayAdapter[i] = jsonUser.getString("name");
//				Log.e("INOUT", "user: " + jsonUser.toString());
				TableRow row = new TableRow(activity);
				row.setId(jsonUser.getInt("id"));
				if (i % 2 == 0) {
					row.setBackgroundColor(Color.LTGRAY);
				}
//				TableLayout.LayoutParams rowLp = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
//						TableLayout.LayoutParams.WRAP_CONTENT);
				row.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
//				row.setLayoutParams(rowLp);
				TextView nameView = new TextView(activity);
				nameView.setText(jsonUser.getString("name"));
				//nameView.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
				TableRow.LayoutParams nameLayoutParams = new TableRow.LayoutParams();
				nameLayoutParams.setMarginEnd(10);
				nameView.setLayoutParams(nameLayoutParams);
				Switch statusSwitch = new Switch(activity);
				statusSwitch.setTextOff("OUT");
				statusSwitch.setTextOn("IN");
				statusSwitch.setId(jsonUser.getInt("id"));
				TableRow.LayoutParams statusLayoutParams = new TableRow.LayoutParams();
				statusLayoutParams.setMarginStart(10);
				statusSwitch.setLayoutParams(statusLayoutParams);
				//statusSwitch.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
				if (jsonUserStatus.getString("status").equals("IN")) {
					statusSwitch.setChecked(true);
				} else {
					statusSwitch.setChecked(false);
				}
				row.addView(statusSwitch);
				row.addView(nameView);
				table.addView(row);
				
				statusSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				        Log.e("INOUT-Submission", "Status changing...");
				        userId = buttonView.getId();
				        String inoutUrlString = PreferenceManager.getDefaultSharedPreferences(activity)
								.getString("pref_server_url", null) + "inoutrecords";				
						new SubmitTask(inoutUrlString).execute();
				    }
				});
				
			}
			ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
					activity, android.R.layout.simple_list_item_1, userArrayAdapter);
		} catch(Exception e) {
			Log.e("INOUT", "Couldn't generate result: " + e);
		} 
		
	}
	
	private class SubmitTask extends AsyncTask<Void, Void, String> {
		String submitUrlString;
		
		SubmitTask(String urlString) {
			this.submitUrlString = urlString;
		}
		
		@Override
		protected String doInBackground(Void... params) {
			String output = null;
			
			try {
				URL url = new URL(submitUrlString);
				HttpURLConnection conn = (HttpURLConnection)url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type","application/json");
				conn.connect();
				Log.e("INOUT-submit", "userId: " + userId);
				String urlParams = "user_id=" + userId;
//				Log.e("INOUT-submit", "url string: " + submitUrlString + urlParams);
//				urlParams += "&timestamp=" + new Date(System.currentTimeMillis());
				BufferedWriter writer = new BufferedWriter(
						new OutputStreamWriter(conn.getOutputStream()));
				writer.write(urlParams);
				writer.flush();
				int resonseCode = conn.getResponseCode();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(conn.getInputStream()));
				output = reader.readLine();
				reader.close();
				writer.close();
			} catch (Exception e) {
				Log.e("INOUT", "Change status failed: " + e);
			} finally {
				
//				finish();
//				Intent intent = new Intent(EmployeeSubmission.this, EmployeesSelection.class);
//				startActivity(intent);
//				startActivity(getIntent());
			}
			return output;
		}
		
		@Override
		protected void onPostExecute(String result) {
			Toast.makeText(activity, activity.getResources().getString(R.string.done), 
					Toast.LENGTH_SHORT).show(); // TODO add IN/OUT at the end of toast text
		}
	}
}
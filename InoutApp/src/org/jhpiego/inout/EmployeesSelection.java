package org.jhpiego.inout;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import org.jhpiego.inout.http.UsersDownloadTask;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

public class EmployeesSelection extends Activity {
	private int userId;
	JSONObject jsonUser;
	
	// FIXME when disconnected the app crashes; fix this
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		setContentView(R.layout.layout_emp_select);

		// Check connectivity
		checkConnection();
		
	}
	
	@Override
	public void onStart() {
		super.onStart();
		Log.e("INOUT-table", "Activity starting...");
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		// Get the data from the server
		String serverUrlString = PreferenceManager.getDefaultSharedPreferences(this)
				.getString("pref_server_url", null);
		Log.e("INOUT-table", "url: " + serverUrlString);
		// Build users auto complete
		new UsersDownloadTask(this).execute(serverUrlString + "users");
	}
	
	@Override
	public void onStop() {
		super.onStop();
		Log.e("INOUT-table", "Activity stopping...");
		
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setUser(JSONObject user) {
		this.jsonUser = user;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.controller, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public boolean loadSettings(MenuItem item) {
		Log.e("INOUT-Main", "Settings item from menu clicked!");
		
		return true;
	}
	
	public void refresh(View view) {
//		finish();
//		startActivity(getIntent());
		recreate();
	}
	
	void checkConnection() {
		ConnectivityManager connMgr = (ConnectivityManager)getSystemService(
				Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = connMgr.getActiveNetworkInfo();
		if (netInfo == null || !netInfo.isConnected()) {
			Toast.makeText(this, getResources().getString(R.string.device_disconnected), 
					Toast.LENGTH_LONG).show();
			Log.e("INOUT", "Your device is disconnected!");
			//finish();
		}
		new Thread(new Runnable() {
			@Override
			public void run() {
				if(!isServerAvailable()) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(EmployeesSelection.this, 
									getResources().getString(R.string.device_disconnected),
									Toast.LENGTH_LONG).show();							
						}
					});
					//finish();
				}
			}
		}).start();
	}
	private boolean isServerAvailable() {
		try {
			String serverUrlString = PreferenceManager.getDefaultSharedPreferences(this)
					.getString("pref_server_url", null);
			URL url = new URL(serverUrlString);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setConnectTimeout(3 * 1000);
			conn.connect();
			if (conn.getResponseCode() != 200)
				return true;
			else
				return false;
		} catch (Exception e) {
			Log.e("INOUT", "Cannot connect to server: " + e);
			return false;
		}
	}
	
}

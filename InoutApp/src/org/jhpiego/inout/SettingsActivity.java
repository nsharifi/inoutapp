package org.jhpiego.inout;

import java.util.Arrays;

import android.app.Activity;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.util.Log;
import android.preference.PreferenceFragment;

public class SettingsActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getFragmentManager()
		.beginTransaction()
		.replace(android.R.id.content, new SettingsFragment())
		.commit();
	}
	
	private static class SettingsFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.preferences);
			
			final ListPreference listPreference = (ListPreference) findPreference("pref_entry_points");

	        // THIS IS REQUIRED IF YOU DON'T HAVE 'entries' and 'entryValues' in your XML
	        //setListPreferenceData(listPreference);
//			listPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
//				
//				@Override
//				public boolean onPreferenceChange(Preference preference, Object newValue) {
//					// TODO Auto-generated method stub
//					
//					Log.e("INOUT-Prefs", "Currently selected item is: " + listPreference.getEntry());
//					return false;
//				}
//			});
//	        listPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
//	            @Override
//	            public boolean onPreferenceClick(Preference preference) {
//
//	                setListPreferenceData(listPreference);
//	                return false;
//	            }
//	        });
		}
		
		protected static void setListPreferenceData(ListPreference lp) {
	        CharSequence[] entries = { "Guesthouse"};
	        CharSequence[] entryValues = {"Guesthouse"};
	        lp.setEntries(entries);
	        //lp.setDefaultValue("1");
	        lp.setEntryValues(entryValues);
		}
	}
	
}
